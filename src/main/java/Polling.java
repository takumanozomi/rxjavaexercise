import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Polling {


    public void execute() throws InterruptedException {

        Random rn = new Random();

        Observable<Integer> ob = Observable.create(emitter -> {
            int value = rn.nextInt(6);
            emitter.onNext(value);
            emitter.onComplete();
        });

        ob.repeatWhen(x -> x.flatMap(o -> Observable.timer(2, TimeUnit.SECONDS)))
                .takeUntil(integer -> {
                    System.out.println("takeUntil: " + integer);
                    return integer == 4;
                })
                .filter(integer -> {
                    System.out.println("Filter: " + integer);
                    return integer == 4;
                })
                .subscribeOn(Schedulers.io())
                .subscribe(integer -> {
                    System.out.println("OnNext: " + integer);
                }, throwable -> {
                }, () -> {
                    synchronized (this) {
                        notifyAll();
                    }
                });

        synchronized (this) {
            wait();
        }

    }
}

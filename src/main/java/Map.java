import io.reactivex.Observable;

public class Map {

    public static void main(String args[]) {
        System.out.println("map");

        Observable<Integer> intOb = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8);
        intOb.map(integer -> integer * 2)
                .map(integer -> "mapped : " + integer)
                .subscribe(s -> {
                    System.out.println(s);
                }, throwable -> {
                }, () -> {
                });
    }
}

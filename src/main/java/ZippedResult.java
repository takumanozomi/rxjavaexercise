public class ZippedResult {

    public String stringValue;
    public int intValue;
    public float floatValue;

    public ZippedResult(int intValue, String stringValue, float floatValue) {
        this.intValue = intValue;
        this.stringValue = stringValue;
        this.floatValue = floatValue;
    }
}

import io.reactivex.Observable;

public class Filter {

    public static void main(String args[]) {
        System.out.println("Filter");

        Observable<Integer> intOb = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8);
        intOb.filter(integer -> integer % 2 == 0)
                .subscribe(integer -> {
                    System.out.println("onNext");
                    System.out.println(String.valueOf(integer));
                }, throwable -> {
                    System.out.println("onError");
                }, () -> {
                    System.out.println("onComplete");
                });


        System.out.println();

//        intOb.filter(integer -> integer % 2 == 0)
//                .toList()
//                .subscribe(list -> {
//                    System.out.println("onNext");
//                    for(int item : list) {
//                        System.out.println(String.valueOf(item));
//                    }
//                }, throwable -> {
//                    System.out.println("onError");
//                });
    }
}

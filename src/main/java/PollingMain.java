import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class PollingMain {

    public static void main(String args[]) throws InterruptedException {
        System.out.println("Polling");
        Polling polling = new Polling();
        polling.execute();
    }
}

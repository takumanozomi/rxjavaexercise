import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MergeAsync {

    Random rnd = new Random();

    public void execute() throws InterruptedException {

        Observable<String> stringOb1 = Observable.mergeArray(addDelay("1"), addDelay("2"), addDelay("3"), addDelay("4"), addDelay("5"));
        Observable<String> stringOb2 = Observable.mergeArray(addDelay("10"), addDelay("20"), addDelay("30"), addDelay("40"), addDelay("50"));

        Observable.merge(stringOb1, stringOb2)
                .subscribeOn(Schedulers.io())
                .subscribe(s -> {
                    System.out.println("onNext");
                    System.out.println(s);
                }, throwable -> {
                    System.out.println("onError");
                }, () -> {
                    System.out.println("onCompete");
                    synchronized (this) {
                        notifyAll();
                    }
                });

        synchronized (this) {
            wait();
        }
    }

    private <T> Observable addDelay(T value) {
        return Observable.just(value).delaySubscription(rnd.nextInt(500), TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io());
    }
}

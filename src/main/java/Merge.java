import io.reactivex.Observable;

public class Merge {

    public static void main(String args[]) {

        System.out.println("Merge");

        Observable<String> stringOb1 = Observable.fromArray("1", "2", "3", "4", "5");
        Observable<String> stringOb2 = Observable.fromArray("10", "20", "30", "40", "50");
        Observable<String> errorOb = Observable.error(new NullPointerException());

        Observable.merge(stringOb1, stringOb2).subscribe(s -> {
            System.out.println("value:" + s);
        }, throwable -> {
            System.out.println("onError 1 + 2");
        }, () -> {
        });

        System.out.println();
        System.out.println("MergeDelayError");

        Observable.mergeDelayError(stringOb1, errorOb, stringOb2).subscribe(s -> {
            System.out.println("value:" + s);
        }, throwable -> {
            System.out.println("onError 1 + 2 + 3");
        }, () -> {
        });
    }
}

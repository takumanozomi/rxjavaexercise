import io.reactivex.Observable;

import static io.reactivex.Observable.create;

public class Create {

    public static void main(String args[]) {

        System.out.println("Create");

        Observable<String> observableA = Observable.create(emitter -> {
            emitter.onNext("A");
            emitter.onComplete();
        });

        Observable<Integer> observableB = create(emitter -> {
            try {
                emitter.onNext(Integer.valueOf("B"));
            } catch (Exception e) {
                emitter.onError(e);
            }
        });

        observableA.subscribe(s -> System.out.println(s),
                throwable -> System.out.println("onError A"),
                () -> System.out.println("onComplete A"));


        System.out.println();

        observableB.subscribe(s -> System.out.println(s),
                throwable -> System.out.println("onError B"),
                () -> System.out.println("onComplete B"));
    }
}

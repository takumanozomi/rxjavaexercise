import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ZipAsync {

    Random rnd = new Random();

    public void execute() throws InterruptedException {

        Observable<Integer> intOb = Observable.mergeArray(addDelay(1), addDelay(2), addDelay(3), addDelay(4), addDelay(5));
        Observable<String> stringOb = Observable.mergeArray(addDelay("1"), addDelay("2"), addDelay("3"), addDelay("4"), addDelay("5"));
        Observable<Float> floatOb = Observable.mergeArray(addDelay(1f), addDelay(2f), addDelay(3f), addDelay(4f), addDelay(5f));

        Observable.zip(intOb, stringOb, floatOb, (intValue, stringValue, floatValue) -> new ZippedResult(intValue, stringValue, floatValue))
                .subscribeOn(Schedulers.io())
                .subscribe(zippedResult -> {
                    System.out.println("onNext");
                    System.out.println(zippedResult.intValue + " : " + zippedResult.stringValue + " : " + zippedResult.floatValue);
                }, throwable -> {
                    System.out.println("onError");
                }, () -> {
                    System.out.println("onCompete");
                    synchronized (this) {
                        notifyAll();
                    }
                });

//        System.out.println();
//        System.out.println("combineLatest");
//
//        Observable.combineLatest(intOb, stringOb, floatOb, (intValue, stringValue, floatValue) -> new ZippedResult(intValue, stringValue, floatValue))
//                .subscribeOn(Schedulers.io())
//                .subscribe(zippedResult -> {
//                    System.out.println("onNext");
//                    System.out.println(zippedResult.intValue + " : " + zippedResult.stringValue + " : " + zippedResult.floatValue);
//                }, throwable -> {
//                    System.out.println("onError");
//                }, () -> {
//                    System.out.println("onCompete");
//                    synchronized (this) {
//                        notifyAll();
//                    }
//                });

        synchronized (this) {
            wait();
        }
    }

    private <T> Observable addDelay(T value) {
        return Observable.just(value).delaySubscription(rnd.nextInt(500), TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io());
    }
}

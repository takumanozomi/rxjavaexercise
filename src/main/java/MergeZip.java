import io.reactivex.Observable;

public class MergeZip {

    public static void main(String args[]) {

        System.out.println("Merge");

        Observable<String> stringOb1 = Observable.fromArray("1", "2", "3", "4", "5");
        Observable<String> stringOb2 = Observable.fromArray("10", "20", "30", "40", "50");

        Observable<Integer> intOb1 = Observable.fromArray(1, 2, 3, 4, 5);
        Observable<Integer> intOb2 = Observable.fromArray(10, 20, 30, 40, 50);

        Observable.zip(Observable.merge(stringOb1, stringOb2), Observable.merge(intOb1, intOb2), (s, integer) -> {
            return new ZippedResult(integer,s,0);
        }).subscribe(r -> {
            System.out.println("onNext");
            System.out.println(r.intValue + " : " + r.stringValue);
        });
    }
}

import io.reactivex.Observable;

public class MergeAsyncMain {
    public static void main(String args[]) throws InterruptedException {
        System.out.println("MergeAsync");
        MergeAsync mergeAsync = new MergeAsync();
        mergeAsync.execute();
    }
}

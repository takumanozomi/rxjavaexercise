import io.reactivex.Observable;

public class Zip {

    public static void main(String args[]) {

        System.out.println("Zip");

        Observable<Integer> intOb = Observable.fromArray(1, 2, 3, 4, 5);
        Observable<String> stringOb = Observable.fromArray("1", "2", "3", "4", "5");
        Observable<Float> floatOb = Observable.fromArray(1.0f, 2.0f, 3.0f, 4.0f, 5.0f);

        Observable.zip(intOb, stringOb, floatOb, (intValue, stringValue, floatValue) -> new ZippedResult(intValue,stringValue,floatValue))
                .subscribe(result -> {
                    System.out.println("onNext");
                    System.out.println(result.intValue + " : " + result.stringValue + " : " + result.floatValue);
                }, throwable -> {
                    System.out.println("onError");
                }, () -> {
                    System.out.println("onCompete");
                });
    }
}

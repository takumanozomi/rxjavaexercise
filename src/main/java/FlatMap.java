import io.reactivex.Observable;

public class FlatMap {

    public static void main(String args[]){
        System.out.println("FlatMap");

        Observable<Integer> intOb = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8);
        intOb.flatMap(integer -> Observable.just(integer * 2))
                .flatMap(integer -> Observable.just("FlatMapped : " + integer))
                .subscribe(s -> {
                    System.out.println(s);
                }, throwable -> {
                }, () -> {
                });

        System.out.println();

//        intOb.map(integer -> Observable.just(integer * 2))
//                .map(integer -> Observable.just("Mapped : " + integer))
//                .subscribe(result -> {
//                    System.out.println(result);
//                }, throwable -> {
//                }, () -> {
//                });
    }
}
